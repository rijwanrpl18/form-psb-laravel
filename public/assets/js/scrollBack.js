jQuery.fn.exists = function () {
    return this.length > 0;
}
function scrollBack(target) {
    if ($(target).exists()) {
        $('html, body').animate({
            scrollTop: $(target).offset().top
        }, 500);
    }
}