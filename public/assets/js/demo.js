// Auto update layout
(function() {
    window.layoutHelpers.setAutoUpdate(true);
})();

// Collapse menu
(function() {
    if (
        $("#layout-sidenav").hasClass("sidenav-horizontal") ||
        window.layoutHelpers.isSmallScreen()
    ) {
        return;
    }

    try {
        window.layoutHelpers.setCollapsed(
            localStorage.getItem("layoutCollapsed") === "true",
            false
        );
    } catch (e) {}
})();

$(function() {
    // Initialize sidenav
    $("#layout-sidenav").each(function() {
        new SideNav(this, {
            orientation: $(this).hasClass("sidenav-horizontal")
                ? "horizontal"
                : "vertical"
        });
    });

    // Initialize sidenav togglers
    $("body").on("click", ".layout-sidenav-toggle", function(e) {
        e.preventDefault();
        window.layoutHelpers.toggleCollapsed();
        if (!window.layoutHelpers.isSmallScreen()) {
            try {
                localStorage.setItem(
                    "layoutCollapsed",
                    String(window.layoutHelpers.isCollapsed())
                );
            } catch (e) {}
        }
    });

    if ($("html").attr("dir") === "rtl") {
        $("#layout-navbar .dropdown-menu").toggleClass("dropdown-menu-right");
    }
});

$(document).ready(function() {
    $(".select2").each(function() {
        $(this)
            .wrap('<div class="position-relative"></div>')
            .select2({
                placeholder: "--Select--",
                dropdownParent: $(this).parent()
            });
    });
    $(".select2-jurnal").each(function() {
        $(this)
            .wrap('<div class="position-relative" style="width: 40%"></div>')
            .select2({
                placeholder: "--Select--",
                dropdownParent: $(this).parent()
            });
    });
    $("ul.nav li.active")
        .eq(0)
        .parents("li.menu")
        .addClass("active");
    $("li.active")
        .parents()
        .addClass("open active");

    $(".datepicker").datepicker({
        autoclose: !0,
        format: "yyyy-mm-dd",
        orientation: "auto"
    });
    $(document).on("keyup", ".money-with-separator", function(e) {
        $(".money-with-separator").mask("000,000,000,000,000,000,000.00", {
            reverse: true
        });
    });
    $(document).on("keyup", ".money-without-separator", function(e) {
        $(".money-without-separator").mask("000,000,000,000,000,000,000", {
            reverse: true
        });
    });
});
