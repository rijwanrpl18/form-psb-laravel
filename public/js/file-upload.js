$(document).ready(function() {
    $(".upload").change(function(e) {
        var fileName = e.target.files[0].name;
        var target = $(this).attr("data-target");
        $("#" + target).html(fileName);
    });
});
