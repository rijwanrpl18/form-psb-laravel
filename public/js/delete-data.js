$(document).ready(function() {
    $(document).on("click", ".data-delete", function() {
        var url = $(this).attr("data-url");
        var message = $(this).attr("data-message");
        var conf = confirm(message);
        if (conf) {
            window.location.replace(url);
        }
    });
});
