<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/desa', 'HomeController@getVillage');

Auth::routes(['verify' => true]);

Route::get('confirm', 'Auth\ConfirmController@index')->name('confirm');

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile', 'HomeController@update')->name('profile.update');
Route::get('/siswa', 'SiswaController@index')->name('siswa');
Route::get('/siswa/delete/{id}', 'SiswaController@delete')->name('siswa.delete');
Route::get('/siswa/{id}', 'SiswaController@show')->name('siswa.show');
Route::get('/siswa/edit/{id}', 'SiswaController@edit')->name('siswa.edit');
Route::post('/siswa/update/{id}', 'SiswaController@update')->name('siswa.update');

Route::get('/download-pdf/{id}', 'PdfController@index')->name('download.pdf');
Route::get('/bulk-download-pdf', 'PdfController@bulkDownload')->name('bulk.download.pdf');
Route::get('/download-excel', 'SiswaController@download')->name('download.excel');

// Route::get('testmail', 'Auth\RegisterController@sendMail');
