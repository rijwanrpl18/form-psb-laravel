<?php

namespace App\Classes;

use App\Http\Requests\UpdateProfile;
use App\Lampiran;
use App\OrangTua;
use App\Periodik;
use App\Prestasi;
use App\Profile;
use App\User;
use App\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Master {
    public function updateSiswa(UpdateProfile $request, $id)
    {
        $data = $request->all();
        // update user
        $user = User::find($id);
        $userUpdate = [
            'nama' => $data['nama'],
            'email' => $data['email']
        ];
        if (!empty($data['change_password'])) {
            $userUpdate['password'] = $data['change_password'];
        }
        $user->update($userUpdate);

        // update profile
        $this->updateProfile($user->id, $data);

        // update Orang tua
        $this->updateOrangTua($user->id, $data);

        // update Periodik
        $this->updatePeriodik($user->id, $data);

        // update Prestasi
        $this->updatePrestasi($user->id, $data);

        // update lampiran
        $this->updateLampiran($request);

        return true;
    }

    private function updateProfile($userid, $data)
    {

        $data = [
            'nama' => $data['nama'],
            'email' => $data['email'],
            'kategori' => $data['kategori'],
            'program' => $data['program'],
            'jk' => $data['jk'],
            'nisn' => $data['nisn'],
            'asal_sekolah' => $data['asal_sekolah'],
            'nik' => $data['nik'],
            'tempat_lahir' => $data['tempat_lahir'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'agama' => $data['agama'],
            'kebutuhan_khusus' => $data['kebutuhan_khusus'],
            'alamat' => $data['alamat'],
            'rt' => $data['rt'],
            'rw' => $data['rw'],
            'kelurahan_id' => $data['kelurahan_id'],
            'kode_pos' => $data['kode_pos'],
            'alat_transportasi' => $data['alat_transportasi'],
            'jenis_tinggal' => $data['jenis_tinggal'],
            'telepon_rumah' => $data['telepon_rumah'],
            'telepon' => $data['telepon'],
        ];
        $kelurahan = Village::find($data['kelurahan_id']);
        $data['kecamatan_id'] = $kelurahan['district_id'];
        $data['kota_id'] = $kelurahan['regency_id'];
        $data['provinsi_id'] = $kelurahan['province_id'];
        $profile = Profile::where('user_id', $userid)->first();
        if ($profile) {
            $profile->update($data);
        } else {
            $data['user_id'] = $userid;
            Profile::create($data);
        }
        return true;
    }

    private function updateOrangTua($userid, $data)
    {
        $data_ortu = [
            'nama_ayah' => $data['nama_ayah'],
            'nik_ayah' => $data['nik_ayah'],
            'tahun_lahir_ayah' => $data['tahun_lahir_ayah'],
            'pekerjaan_ayah' => $data['pekerjaan_ayah'],
            'penghasilan_ayah' => $data['penghasilan_ayah'],
            'pendidikan_ayah' => $data['pendidikan_ayah'],
            'telepon_ayah' => $data['telepon_ayah'],
            'nama_ibu' => $data['nama_ibu'],
            'nik_ibu' => $data['nik_ibu'],
            'tahun_lahir_ibu' => $data['tahun_lahir_ibu'],
            'pekerjaan_ibu' => $data['pekerjaan_ibu'],
            'penghasilan_ibu' => $data['penghasilan_ibu'],
            'pendidikan_ibu' => $data['pendidikan_ibu'],
            'telepon_ibu' => $data['telepon_ibu'],
            'nama_wali' => $data['nama_wali'],
            'nik_wali' => $data['nik_wali'],
            'tahun_lahir_wali' => $data['tahun_lahir_wali'],
            'pekerjaan_wali' => $data['pekerjaan_wali'],
            'penghasilan_wali' => $data['penghasilan_wali'],
            'pendidikan_wali' => $data['pendidikan_wali'],
            'telepon_wali' => $data['telepon_wali'],
        ];
        $ortu = OrangTua::where('user_id', $userid)->first();
        if ($ortu) {
            $ortu->update($data_ortu);
        } else {
            $data_ortu['user_id'] = $userid;
            OrangTua::create($data_ortu);
        }
        return true;
    }

    private function updatePeriodik($userid, $data)
    {
        $data = [
            'tinggi_badan' => $data['tinggi_badan'],
            'berat_badan' => $data['berat_badan'],
            'lingkar_kepala' => $data['lingkar_kepala'],
            'jarak' => $data['jarak'],
            'waktu_tempuh' => $data['waktu_tempuh'],
            'jumlah_saudara' => $data['jumlah_saudara'],
        ];
        $periodik = Periodik::where('user_id', $userid)->first();
        if ($periodik) {
            $periodik->update($data);
        } else {
            $data['user_id'] = $userid;
            Periodik::create($data);
        }
        return true;
    }

    private function updatePrestasi($userid, $data)
    {
        $data = [
            'nama_prestasi' => $data['nama_prestasi'],
            'tingkat' => $data['tingkat'],
            'juara' => $data['juara'],
            'tahun' => $data['tahun'],
        ];
        $prestasi = Prestasi::where('user_id', $userid)->first();
        if ($prestasi) {
            $prestasi->update($data);
        } else {
            $data['user_id'] = $userid;
            Prestasi::create($data);
        }
        return true;
    }

    private function updateLampiran(Request $request)
    {
        $lampiran = Lampiran::where('user_id', auth()->id())->first();
        $data = [];
        if ($lampiran) {
            if ($request->has('foto')) {
                //upload new foto to storage/app/public/foto
                if (!empty($lampiran->foto)) Storage::delete($lampiran->foto);
                $data['foto'] = $request->file('foto')->store('foto');
            }
            if ($request->has('akte')) {
                //upload new akte to storage/app/public/akte
                if (!empty($lampiran->akte)) Storage::delete($lampiran->akte);
                $data['akte'] = $request->file('akte')->store('akte');
            }
            if ($request->has('kk')) {
                //upload new kk to storage/app/public/kk
                if (!empty($lampiran->kk)) Storage::delete($lampiran->kk);
                $data['kk'] = $request->file('kk')->store('kk');
            }
            if ($request->has('badge')) {
                //upload new badge to storage/app/public/badge
                if (!empty($lampiran->badge)) Storage::delete($lampiran->badge);
                $data['badge'] = $request->file('badge')->store('badge');
            }
            if ($request->has('skl')) {
                //upload new skl to storage/app/public/skl
                if (!empty($lampiran->skl)) Storage::delete($lampiran->skl);
                $data['skl'] = $request->file('skl')->store('skl');
            }
            $lampiran->update($data);
        } else {
            $data['user_id'] = auth()->id();
            if ($request->has('foto')) {
                //upload new foto to storage/app/public/foto
                $data['foto'] = $request->file('foto')->store('foto');
            }
            if ($request->has('ktp')) {
                //upload new ktp to storage/app/public/ktp
                $data['ktp'] = $request->file('ktp')->store('ktp');
            }
            if ($request->has('kk')) {
                //upload new kk to storage/app/public/kk
                $data['kk'] = $request->file('kk')->store('kk');
            }
            if ($request->has('badge')) {
                //upload new badge to storage/app/public/badge
                $data['badge'] = $request->file('badge')->store('badge');
            }
            if ($request->has('skl')) {
                //upload new skl to storage/app/public/skl
                $data['skl'] = $request->file('skl')->store('skl');
            }
            Lampiran::create($data);
        }

        return true;
    }
}
