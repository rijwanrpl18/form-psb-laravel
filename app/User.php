<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

// class User extends Authenticatable implements MustVerifyEmail
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $guarded = ['id'];

    protected $with = [
        'profile',
        'orangtua',
        'periodik',
        'prestasi',
        'lampiran'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function orangtua()
    {
        return $this->hasOne(OrangTua::class);
    }

    public function periodik()
    {
        return $this->hasOne(Periodik::class);
    }

    public function prestasi()
    {
        return $this->hasOne(Prestasi::class);
    }

    public function lampiran()
    {
        return $this->hasOne(Lampiran::class);
    }
}
