<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function periodik()
    {
        return $this->hasOne(Periodik::class, 'user_id', 'user_id');
    }

    public function orangtua()
    {
        return $this->hasOne(OrangTua::class, 'user_id', 'user_id');
    }

    public function prestasi()
    {
        return $this->hasOne(Prestasi::class, 'user_id', 'user_id');
    }

    public function lampiran()
    {
        return $this->hasOne(Lampiran::class, 'user_id', 'user_id');
    }
}
