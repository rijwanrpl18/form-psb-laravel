<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['nullable', 'string', 'min:8'],
            'nisn' => 'required',
            'kategori' => 'required',
            'program' => 'required',
            'asal_sekolah' => 'required',
            'agama' => 'required',
            'kelurahan_id' => 'required',
            'telepon' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'foto' => 'nullable|mimes:jpg,jpeg,png',
            'akte' => 'nullable|mimes:jpg,jpeg,png,pdf',
            'kk' => 'nullable|mimes:jpg,jpeg,png,pdf',
            'badge' => 'nullable|mimes:jpg,jpeg,png,pdf',
            'skl' => 'nullable|mimes:jpg,jpeg,png,pdf',
        ];
    }
}
