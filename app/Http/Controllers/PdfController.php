<?php

namespace App\Http\Controllers;

use App\User;
use App\Village;
use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class PdfController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index(Request $request, $id)
    {
        if (auth()->user()->role == 'siswa') {
            $id = auth()->user()->id;
        }

        $data['data'] = User::findOrFail($id);
        $data['active_menu'] = 'siswa';
        $data['master'] = config('base');
        $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);

        $pdf = PDF::loadView('download', compact('data'));

        return $pdf->download(Carbon::now()->format('YmdHis') . '-' . $data['data']->nama . '.pdf');
    }

    public function bulkDownload(Request $request)
    {
        if (auth()->user()->role == 'siswa') {
            $id = auth()->user()->id;
        }
        $time = Carbon::now()->format('YmdHis');
        Storage::disk('local')->makeDirectory('siswa' . $time, $mode = 0775, true, true); // zip store here

        $siswa = User::where('role', 'siswa')->get();
        foreach ($siswa as $key => $value) {
            $data['data'] = $value;
            $data['active_menu'] = 'siswa';
            $data['master'] = config('base');
            $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);

            $pdf = PDF::loadView('download', compact('data'));
            $content = $pdf->download()->getOriginalContent();

            Storage::put('siswa' . $time . '/' . $data['data']->nama . '.pdf', $content);
        }

        $zip = new ZipArchive();
        $tempFile = tmpfile();
        $tempFileUri = stream_get_meta_data($tempFile)['uri'];
        if ($zip->open($tempFileUri, ZipArchive::CREATE) === TRUE) {
            $files = File::files(public_path('storage/siswa' . $time));
            foreach ($files as $key => $value) {
                $relativeName = basename($value);
                $zip->addFile($value, $relativeName);
            }
            $zip->close();
        }
        $fileName = 'siswa' . $time . '.zip';
        rename($tempFileUri, $fileName);
        Storage::deleteDirectory('siswa' . $time);
        return response()->download(public_path('siswa' . $time . '.zip'));
    }

    public function excelDownload(Request $request)
    {
        if (auth()->user()->role == 'siswa') {
            $id = auth()->user()->id;
        }
        $time = Carbon::now()->format('YmdHis');
        Storage::disk('local')->makeDirectory('siswa' . $time, $mode = 0775, true, true); // zip store here

        $siswa = User::where('role', 'siswa')->get();
        foreach ($siswa as $key => $value) {
            $data['data'] = $value;
            $data['active_menu'] = 'siswa';
            $data['master'] = config('base');
            $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);

            $pdf = PDF::loadView('download', compact('data'));
            $content = $pdf->download()->getOriginalContent();

            Storage::put('siswa' . $time . '/' . $data['data']->nama . '.pdf', $content);
        }

        $zip = new ZipArchive();
        $tempFile = tmpfile();
        $tempFileUri = stream_get_meta_data($tempFile)['uri'];
        if ($zip->open($tempFileUri, ZipArchive::CREATE) === TRUE) {
            $files = File::files(public_path('storage/siswa' . $time));
            foreach ($files as $key => $value) {
                $relativeName = basename($value);
                $zip->addFile($value, $relativeName);
            }
            $zip->close();
        }
        $fileName = 'siswa' . $time . '.zip';
        rename($tempFileUri, $fileName);
        Storage::deleteDirectory('siswa' . $time);
        return response()->download(public_path('siswa' . $time . '.zip'));
    }
}
