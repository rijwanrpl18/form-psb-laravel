<?php

namespace App\Http\Controllers;

use App\Classes\Master;
use App\Exports\SiswaExport;
use App\Http\Requests\UpdateProfile;
use App\Profile;
use App\User;
use App\Village;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SiswaController extends Controller
{
    private $master;
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->master = new Master();
    }

    public function index(Request $request)
    {
        if (auth()->user()->role == 'siswa') {
            return redirect()->route('home');
        }

        $data['q'] = $_GET['q'] ?? '';
        $data['limit'] = $_GET['limit'] ?? 'all';
        
        $model = Profile::with('lampiran');
        if (!empty($data['q'])) {
            $model = $model->where(function ($q) use ($data) {
                $q->where("nama", "like", "%{$data['q']}%")
                    ->orWhere("email", "like", "%{$data['q']}%")
                    ->orWhere("asal_sekolah", "like", "%{$data['q']}%")
                    ->orWhere("nik", "like", "%{$data['q']}%")
                    ->orWhere("nisn", "like", "%{$data['q']}%")
                    ->orWhere("alamat", "like", "%{$data['q']}%")
                    ->orWhere("telepon", "like", "%{$data['q']}%")
                    ->orWhere("tempat_lahir", "like", "%{$data['q']}%");
            });
        }
        if ($data['limit'] == 'all'){
            $data['data'] = $model->get();
        } else {
            $data['data'] = $model->paginate($data['limit']);
        }

        $data['active_menu'] = 'siswa';
        $data['breadcrumb'] = [
            'Data Siswa' => route('siswa')
        ];

        return view('siswa', compact('data'));
    }

    public function show(Request $request, $id)
    {
        $data['data'] = User::findOrFail($id);
        $data['active_menu'] = 'siswa';
        $data['master'] = config('base');
        $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);
        $data['breadcrumb'] = [
            'Data Siswa' => route('siswa'),
            $data['data']->nama => route('siswa.show', ['id', $id])
        ];
        return view('detail', compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data['data'] = User::with(['profile', 'orangtua', 'periodik', 'prestasi', 'lampiran'])->find($id);
        $data['master'] = config('base');
        $data['active_menu'] = 'siswa';
        $data['breadcrumb'] = [
            'Data Siswa' => route('siswa'),
            'Edit ' . $data['data']->nama => route('siswa.edit', ['id' => $id])
        ];
        $data['desa'] = [];
        if ($data['data']->profile != null) {
            $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);
        }
        return view('form', compact('data'));
    }

    public function update(UpdateProfile $request)
    {
        $id = $request->input('id');
        $this->master->updateSiswa($request, $id);
        return redirect()->route('siswa')->with(['success' => 'Data siswa berhasil diperbaharui.']);
    }

    public function download(Request $req)
    {
        ini_set('memory_limit', '-1');
        $siswa = Profile::with(['user', 'orangtua', 'periodik', 'prestasi'])->get();
        $config = config('base');
        $data  = [];
        foreach ($siswa as $key => $value) {
            $desa = Village::with(['district', 'regency', 'province'])->find($value->kelurahan_id);
            $data[] = [
                'kategori' => $config['kategori_pendaftar'][$value->kategori] ?? '',
                'nama' => $value->nama,
                'nisn' => $value->nisn,
                'program' => $config['kategori_pendaftar'][$value->program] ?? '',
                'jenis_kelamin' => $value->jk ? 'Laki-Laki' : 'Perempuan',
                'asal_sekolah' => $value->asal_sekolah,
                'nik' => $value->nik,
                'tempat_lahir' => $value->tempat_lahir,
                'tanggal_lahir' => $value->tanggal_lahir,
                'agama' => $config['agama'][$value->agama] ?? '',
                'kebutuhan_khusus' => $value->kebutuhan_khusus ? 'Ya' : 'Tidak',
                'alamat' => $value->alamat,
                'rt' => $value->rt,
                'rw' => $value->rw,
                'kelurahan' => $desa->name ?? '',
                'kecamatan' => $desa->district->name ?? '',
                'kota' => $desa->regency->name ?? '',
                'provinsi' => $desa->province->name ?? '',
                'kode_pos' => $value->kode_pos,
                'alat_transportasi' => $config['alat_transportasi'][$value->alat_transportasi] ?? '',
                'jenis_tinggal' => $config['jenis_tinggal'][$value->jenis_tinggal] ?? '',
                'telepon_rumah' => $value->telepon_rumah,
                'telepon' => $value->telepon,
                'email' => $value->email,

                // data ayah
                'nama_ayah' => $value->orangtua != null ? $value->orangtua->nama_ayah : '',
                'nik_ayah' => $value->orangtua != null ? $value->orangtua->nik_ayah : '',
                'tahun_lahir_ayah' => $value->orangtua != null ? $value->orangtua->tahun_lahir_ayah : '',
                'pendidikan_ayah' => $value->orangtua != null ? $value->orangtua->pendidikan_ayah : '',
                'pekerjaan_ayah' => $value->orangtua != null ? $value->orangtua->pekerjaan_ayah : '',
                'penghasilan_ayah' => $value->orangtua != null ? $value->orangtua->penghasilan_ayah : '',
                'telepon_ayah' => $value->orangtua != null ? $value->orangtua->telepon_ayah : '',

                // data ibu
                'nama_ibu' => $value->orangtua != null ? $value->orangtua->nama_ibu : '',
                'nik_ibu' => $value->orangtua != null ? $value->orangtua->nik_ibu : '',
                'tahun_lahir_ibu' => $value->orangtua != null ? $value->orangtua->tahun_lahir_ibu : '',
                'pendidikan_ibu' => $value->orangtua != null ? $value->orangtua->pendidikan_ibu : '',
                'pekerjaan_ibu' => $value->orangtua != null ? $value->orangtua->pekerjaan_ibu : '',
                'penghasilan_ibu' => $value->orangtua != null ? $value->orangtua->penghasilan_ibu : '',
                'telepon_ibu' => $value->orangtua != null ? $value->orangtua->telepon_ibu : '',

                // data wali
                'nama_wali' => $value->orangtua != null ? $value->orangtua->nama_wali : '',
                'nik_wali' => $value->orangtua != null ? $value->orangtua->nik_wali : '',
                'tahun_lahir_wali' => $value->orangtua != null ? $value->orangtua->tahun_lahir_wali : '',
                'pendidikan_wali' => $value->orangtua != null ? $value->orangtua->pendidikan_wali : '',
                'pekerjaan_wali' => $value->orangtua != null ? $value->orangtua->pekerjaan_wali : '',
                'penghasilan_wali' => $value->orangtua != null ? $value->orangtua->penghasilan_wali : '',
                'telepon_wali' => $value->orangtua != null ? $value->orangtua->telepon_wali : '',

                // data prestasi
                'nama_prestasi' => $value->prestasi != null ? $value->prestasi->nama_prestasi : '',
                'tingkat' => $value->prestasi != null ? $value->prestasi->tingkat : '',
                'juara' => $value->prestasi != null ? $value->prestasi->juara : '',
                'tahun' => $value->prestasi != null ? $value->prestasi->tahun : '',

                // data periodik
                'tinggi_badan' => $value->periodik != null ? $value->periodik->tinggi_badan . ' cm' : '',
                'berat_badan' => $value->periodik != null ? $value->periodik->berat_badan . ' kg' : '',
                'lingkar_kepala' => $value->periodik != null ? $value->periodik->lingkar_kepala . ' cm' : '',
                'jarak' => $value->periodik != null ? $value->periodik->jarak . ' meter' : '',
                'waktu_tempuh' => $value->periodik != null ? $value->periodik->waktu_tempuh . ' menit' : '',
                'jumlah_saudara' => $value->periodik != null ? $value->periodik->jumlah_saudara . ' orang' : ''
            ];
        }

        // dd($data);
        return Excel::download(new SiswaExport($data), 'Data Siswa.xlsx');
    }

    public function delete($id)
    {
        User::where('id', $id)->delete();
        Profile::where('user_id', $id)->delete();
        return redirect(route('siswa'));
    }
}
