<?php

namespace App\Http\Controllers;

use App\Classes\Master;
use App\Http\Requests\UpdateProfile;
use App\Village;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $master;
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->master = new Master();
    }

    public function index()
    {
        $data['data'] = auth()->user();
        if ($data['data']->role == 'siswa') {
            $data['master'] = config('base');
            $data['desa'] = [];
            if ($data['data']->profile != null) {
                $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);
            }
        } else {
            return redirect(route('siswa'));
        }
        $data['active_menu'] = 'home';
        $data['breadcrumb'] = [];
        return view('home', compact('data'));
    }

    public function getVillage(Request $request)
    {
        if ($request->has('q')) {
            // $desa = $this->area->villageList(['q' => $request->q]);
            $desa = Village::where('name', 'like', "%$request->q%")->get();
            $data = [];
            $i = 0;
            foreach ($desa as $key => $value) {
                $data[$i]['id'] = $value->id;
                $data[$i]['name'] = $value->name;
                $data[$i]['district_name'] = $value->district->name;
                $data[$i]['regency_name'] = $value->regency->name;
                $data[$i]['province_name'] = $value->province->name;
                $i++;
            }
            return response()->json($data);
        }
    }

    public function profile()
    {
        $data['data'] = auth()->user();
        if ($data['data']->role == 'admin') {
            return redirect()->route('home');
        }
        $data['master'] = config('base');
        $data['active_menu'] = 'profile';
        $data['breadcrumb'] = [
            'Edit Profile' => route('profile')
        ];
        $data['desa'] = [];
        if ($data['data']->profile != null) {
            $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);
        }

        return view('profile', compact('data'));
    }

    public function update(UpdateProfile $request)
    {
        $id = auth()->id();
        $this->master->updateSiswa($request, $id);
        return redirect()->route('home')->with(['success' => 'Profile berhasil diperbaharui.']);
    }
}
