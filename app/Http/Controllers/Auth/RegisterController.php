<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Lampiran;
use App\OrangTua;
use App\Periodik;
use App\Prestasi;
use App\Profile;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $message = [
            'required' => 'Field :attribute tidak boleh kosong'
        ];
        return Validator::make($data, [
            'nama' => ['required', 'string', 'max:255'],
            'asal_sekolah' => ['required'],
            'no_hp' => ['required'],
            'program' => ['required', 'int'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string'],
        ], $message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'nama' => $data['nama'],
            'email' => $data['email'],
            'password' => $data['password']
        ]);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $data = $request->all();
        $data['tanggal_lahir'] = implode('-', [$data['tahun_lahir'], $data['bulan_lahir'], $data['tanggal_lahir']]);
        // create profile
        Profile::create([
            'user_id' => $user->id,
            'program' => $data['program'],
            'kategori' => $data['kategori'],
            'nama' => $data['nama'],
            'email' => $data['email'],
            'no_hp' => $data['no_hp'],
            'jk' => $data['jk'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'tempat_lahir' => $data['tempat_lahir'],
            'asal_sekolah' => $data['asal_sekolah'],
            'telepon' => $data['no_hp'],
        ]);
        // Create orang tua
        OrangTua::create([
            'user_id' => $user->id
        ]);
        // Create periodik
        Periodik::create([
            'user_id' => $user->id
        ]);
        // Create prestasi
        Prestasi::create([
            'user_id' => $user->id
        ]);
        // Create lampiran
        Lampiran::create([
            'user_id' => $user->id
        ]);
    }
}
