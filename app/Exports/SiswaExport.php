<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class SiswaExport implements FromArray, WithHeadings, WithEvents, ShouldAutoSize
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            [
                'Detail Siswa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
                'Data Ayah', '', '', '', '', '', '',
                'Data Ibu', '', '', '', '', '', '',
                'Data Wali', '', '', '', '', '', '',
                'Data Prestasi', '', '', '',
                'Data Periodik', '', '', '', '', ''
            ],
            [
                'Kategori Pendaftaran', // A
                'Nama',
                'NISN',
                'Program',
                'Jenis Kelamin', // E
                'Asal Sekolah',
                'NIK',
                'Tempat Lahir',
                'Tanggal Lahir',
                'Agama', //J
                'Berkebutuhan Khusus',
                'Alamat',
                'RT',
                'RW',
                'Kelurahan/Desa', //O
                'Kecamatan',
                'Kabupaten/Kota',
                'Provinsi',
                'Kode Pos',
                'Alat Transportasi ke Sekolah', // T
                'Jenis Tinggal',
                'No Telepon Rumah',
                'No HP/WA Siswa',
                'Email', // X

                // Data Ayah
                'Nama', // Y
                'NIK',
                'Tahun Lahir',
                'Jenjan Pendidikan',
                'Pekerjaan',
                'Penghasilan Bulanan',
                'No HP/WA', //AE

                // Data Ibu
                'Nama', // AF
                'NIK',
                'Tahun Lahir',
                'Jenjan Pendidikan',
                'Pekerjaan',
                'Penghasilan Bulanan',
                'No HP/WA', // AL

                // Data Wali
                'Nama', // AM
                'NIK',
                'Tahun Lahir',
                'Jenjan Pendidikan',
                'Pekerjaan',
                'Penghasilan Bulanan',
                'No HP/WA', // AS

                // Data Prestasi
                'Nama Prestasi', // AT
                'Tingkat',
                'Juara Ke',
                'Tahun', // AW

                // Data Periodik
                'Tinggi Badan', // AX
                'Berat Badan',
                'Lingkar Kepala',
                'Jarak Tempat Tinggal ke Sekolah',
                'Waktu Tempuh Berangkat ke Sekolah',
                'Jumlah Saudara Kandung' // BC
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event){
                $event->sheet->mergeCells("A1:X1");
                $event->sheet->mergeCells("Y1:AE1");
                $event->sheet->mergeCells("AF1:AL1");
                $event->sheet->mergeCells("AM1:AS1");
                $event->sheet->mergeCells("AT1:AW1");
                $event->sheet->mergeCells("AX1:BC1");
                $event->sheet->getStyle('A1:BC1')->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        ];
    }
}
