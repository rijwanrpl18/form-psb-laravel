<?php
return [
    'kategori_pendaftar' => [
        'Umum',
        'PT Badak',
        'Pensiunan PT Badak',
        'YPVDP',
        'Mitra PT Badak',
    ],
    'program' => [
        'Reguler',
        'Kelas Tahfidz',
        'Kelas IT'
    ],
    'agama' => [
        'Islam',
        'Kristen',
        'Katholik',
        'Hindu',
        'Budha'
    ],
    'alat_transportasi' => [
        'Umum',
        'Kendaraan Pribadi',
        'Antar Jemput',
    ],
    'jenis_tinggal' => [
        'Lainnya',
        'Bersama Orang Tua',
        'Bersama Kerabat'
    ],
    'pendidikan' => [
        'Tidak Sekolah',
        'SD',
        'SMP/MTs',
        'SMA/SMK/MA',
        'S1',
        'S2'
    ],
    'pekerjaan' => [
        'Tidak Bekerja',
        'PNS',
        'TNI/POLRI',
        'Karyawan Swasta',
        'Wiraswasta',
        'BUMN',
        'Pensiunan',
    ],
    'penghasilan' => [
        '< 2jt',
        '2jt - 4.9jt',
        '5jt - 9.9jt',
        '> 10jt'
    ]
];
