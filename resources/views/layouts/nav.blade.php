<!-- Layout sidenav -->
<div
    id="layout-sidenav"
    class="layout-sidenav sidenav sidenav-vertical bg-dark">
    <div class="app-brand demo">
        <img
            src="{{ asset('assets/img/logo.png') }}"
            alt=""
            class="app-brand-logo demo">
        <a
            href="#"
            class="app-brand-text demo sidenav-text font-weight-normal ml-2">PSB</a>
        <a
            href="javascript:void(0)"
            class="layout-sidenav-toggle sidenav-link text-large ml-auto">
            <i class="fa fa-bars align-middle"></i>
        </a>
    </div>

    <div class="sidenav-divider mt-0"></div>

    <!-- Links -->
    <ul class="sidenav-inner py-1">
        <!-- HOME -->
        @if (auth()->user()->role == 'siswa')
        <li class="sidenav-item {{ $data['active_menu'] == 'home' ? 'active' : '' }}">
            <a href="{{ route('home') }}" class="sidenav-link" >
                <i class="sidenav-icon fa fa-home"></i>
                <div>Home</div>
            </a>
        </li>
        @endif
        <!-- Siswa -->
        @if (auth()->user()->role == 'admin')
            <li class="sidenav-item {{ $data['active_menu'] == 'siswa' ? 'active' : '' }}">
                <a href="{{ route('siswa') }}" class="sidenav-link" >
                    <i class="sidenav-icon fa fa-user"></i>
                    <div>Data Siswa</div>
                </a>
            </li>
        @endif
        <!-- PROFILE -->
        @if (auth()->user()->role == 'siswa')
            <li class="sidenav-item {{ $data['active_menu'] == 'profile' ? 'active' : '' }}">
                <a href="{{ route('profile') }}" class="sidenav-link" >
                    <i class="sidenav-icon fa fa-user-edit"></i>
                    <div>Edit Profile</div>
                </a>
            </li>
        @endif

        <!-- LOGOUT -->
        <li class="sidenav-item">
            <a href="{{ route('logout') }}" class="sidenav-link" >
                <i class="sidenav-icon fa fa-sign-out-alt"></i>
                <div>Logout</div>
            </a>
        </li>
    </ul>
</div>
<!-- / Layout sidenav -->
