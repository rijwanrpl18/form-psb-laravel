@extends('layouts.application')
@section('module', 'Data Siswa')

@section('content')
<div class="ui-bordered px-3 pt-3 mb-3">
    <form
        class="form-row align-items-center"
        method="get"
        action="{{ url()->current() }}">
        <div class="col-md-2 mb-3">
            <label class="form-label">Limit</label>
            <select class="select2 form-control" name="limit">
                <option value="all" {{ $data['limit'] == 'all' ? 'selected' : '' }}>Show All</option>
                <option value="20" {{ $data['limit'] == 20 ? 'selected' : '' }}>20</option>
                <option value="50" {{ $data['limit'] == 50 ? 'selected' : '' }}>50</option>
                <option value="100" {{ $data['limit'] == 100 ? 'selected' : '' }}>100</option>
                <option value="150" {{ $data['limit'] == 150 ? 'selected' : '' }}>150</option>
                <option value="200" {{ $data['limit'] == 200 ? 'selected' : '' }}>200</option>
            </select>
        </div>
        <div class="col-md-4 mb-3">
            <label class="form-label">Pencarian</label>
            <div class="input-group">
                <input
                    type="text"
                    class="form-control"
                    placeholder="Kata Kunci"
                    name="q"
                    value="{{ $data['q'] }}">
                <span class="input-group-append">
                    <button
                        class="btn btn-secondary"
                        type="submit">Cari</button>
                </span>
                @if (!empty($data['q']))
                    <span class="input-group-append">
                        <a
                            class="btn btn-danger"
                            href="{{ url()->current() }}">
                            <i class="fa fa-times"></i>
                        </a>
                    </span>
                @endif
            </div>
        </div>

        <div class="col-md text-right">
            <a
                href="{{ route('download.excel') }}"
                class="btn btn-success"
                data-toggle="tooltip"
                data-placement="top"
                data-state="dark"
                title="Download Siswa">
                <i class="fa fa-download"></i> Donwnload Excel
            </a>
            <a
                href="{{ route('bulk.download.pdf') }}"
                class="btn btn-success"
                data-toggle="tooltip"
                data-placement="top"
                data-state="dark"
                title="Download Siswa">
                <i class="fa fa-download"></i> Donwnload PDF
            </a>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header h4 text-center">
                Data Siswa
            </div>
            <div class="table-responsive">
                <table class="table card-table">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>NISN</th>
                            <th>Email</th>
                            <th>Jenis Kelamin</th>
                            <th>Asal Sekolah</th>
                            <th>No Telepon</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            if ($data['limit'] != 'all') {
                                $i = ($data['data']->currentPage() - 1) * $data['data']->perPage();
                            } else {
                                $i = 0;
                            }
                        @endphp
                        @foreach ($data['data'] as $value)
                            @php
                                $i++;
                            @endphp
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $value->nama }}</td>
                                <td>{{ $value->nisn }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->jk ? 'Laki-Laki' : 'Perempuan' }}</td>
                                <td>{{ $value->asal_sekolah }}</td>
                                <td>{{ $value->telepon }}</td>
                                <td class="text-center">
                                    <a
                                        href="{{ route('siswa.show', ['id' => $value->user_id]) }}"
                                        class="btn icon-btn btn-primary btn-sm"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-state="dark"
                                        title="Detail Siswa">
                                        <i class="fa fa-user"></i>
                                    </a>
                                    <a
                                        href="{{ route('siswa.edit', ['id' => $value->user_id]) }}"
                                        class="btn icon-btn btn-warning btn-sm"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-state="dark"
                                        title="Edit Data Siswa">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a
                                        href="{{ route('siswa.delete', ['id' => $value->user_id]) }}"
                                        class="btn icon-btn btn-danger btn-sm"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-state="dark"
                                        title="Delete Data Siswa">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    {{-- <a
                                        href="{{ route('download.pdf', ['id' => $value->user_id]) }}"
                                        class="btn icon-btn btn-success btn-sm"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-state="dark"
                                        title="Dwonload Detail Data Siswa">
                                        <i class="fa fa-download"></i>
                                    </a> --}}
                                    <div
                                        class="btn-group"
                                        role="group">
                                        <button
                                            id="download"
                                            type="button"
                                            class="btn btn-success btn-sm dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false">
                                            <i class="fa fa-download"></i>
                                        </button>
                                        <div
                                            class="dropdown-menu"
                                            aria-labelledby="download">
                                            <a
                                                class="dropdown-item"
                                                href="{{ route('download.pdf', ['id' => $value->user_id]) }}">
                                                Data Siswa
                                            </a>
                                            @if ($value->lampiran->akte)
                                                <a
                                                    class="dropdown-item"
                                                    target="_blank"
                                                    href="{{ asset('storage/'.$value->lampiran->akte) }}">
                                                    Akte Kelahiran
                                                </a>
                                            @endif
                                            @if ($value->lampiran->kk)
                                                <a
                                                    class="dropdown-item"
                                                    target="_blank"
                                                    href="{{ asset('storage/'.$value->lampiran->kk) }}">
                                                    Kartu Keluarga
                                                </a>
                                            @endif
                                            @if ($value->lampiran->badge)
                                                <a
                                                    class="dropdown-item"
                                                    target="_blank"
                                                    href="{{ asset('storage/'.$value->lampiran->badge) }}">
                                                    Badge
                                                </a>
                                            @endif
                                            @if ($value->lampiran->foto)
                                                <a
                                                    class="dropdown-item"
                                                    target="_blank"
                                                    href="{{ asset('storage/'.$value->lampiran->foto) }}">
                                                    Foto
                                                </a>
                                            @endif
                                            @if ($value->lampiran->skl)
                                                <a
                                                    class="dropdown-item"
                                                    target="_blank"
                                                    href="{{ asset('storage/'.$value->lampiran->skl) }}">
                                                    Surat Keterangan Lulus
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-3">
                        @if ($data['limit'] != 'all')
                            Total Record : <strong>{{ $data['data']->count() + ($data['limit']*($data['data']->currentPage() - 1)) }}</strong> of <strong>{{ $data['data']->total() }}</strong>
                        @else
                            Total Record : <strong>{{ $data['data']->count() }}</strong>
                        @endif
                    </div>
                    @if ($data['limit'] != 'all')
                        <div class="col-md-9">
                            {{ $data['data']->appends(request()->input())->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
