<!DOCTYPE html>
<!--
   To change this license header, choose License Headers in Project Properties.
   To change this template file, choose Tools | Templates
   and open the template in the editor.
   -->
<HTML>
   <head>
      <meta charset="UTF-8">
      <title>PSB</title>
      <meta name="description" content="PSB hadir sebagai Platform pendidikan jaman Now, PSB merupakan aplikasi pendidikan, dimana PSB memudahkan orang tua untuk memonitoring kegiatan, nilai, pembayaran sekolah anak.">
      <meta name="keywords" content="PSB, aplikasi pendidikan, platform pendidikan, pembayaran sekolah, sistem akademik, aplikasi sekolah">
      <meta name="author" content="PSB">
      <meta name="viewport" content="width=device-width, maximum-scale=1.0">
      <link rel="shortcut icon" type="img/ico" href="/assets/images/favicon.ico">
      <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}">
      <script src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
      <script src="{{ asset('assets/js/jquery-ui.js') }}"></script>
      <script src="{{ asset('assets/js/app.js') }}"></script>
   </head>
   <body>
      <style>
         .panel-content .panel-heading{
         padding: 10px 25px;
         }
         @media (min-width: 987px){
         .form-horizontal .control-label{
         text-align: left;
         padding-left: 80px;
         }
         }
      </style>
      <div class="body-bg-grey">
         <div class="container">
            <div class="content">
               <h4 style="margin-top: 30px;" class="title text-center">FORMULIR PENDAFTARAN</h4>
               <br>
                @if (Session::get('success'))
                    <div class="alert alert-dark-success alert-dismissible fade show">
                        <i class="fa fa-check"></i>
                        {!! session('success') !!}
                    </div>
                @endif
               <div class="alert alert-content alert-blue">
                  <img class="images" src="https://pmb.alazhar-yogyakarta.com/assets/images/templates/alaram.png" />
                  <div class="desc">
                     <h5 class="title">HARAP DIPERHATIKAN..!!</h5>
                     <p>1. Pastikan Anda telah Membaca Semua Aturan Pendaftaran yang berlaku di masing-masing sekolah..</p>
                     <p>2. Isi Data Berikut Dengan Benar</p>
                     <p>3. Hubungi Customer Service Online Kami Bila Anda Mengalami Kesulitan.</p>
                  </div>
               </div>
               <form method="POST" action="{{ route('register') }}" accept-charset="UTF-8" class="form-horizontal form-conten form-validation">
                    @csrf
                  <div class="panel panel-content">
                     <div id="identitas">
                        <div class="panel-heading panel-head-grey">IDENTITAS CALON SISWA</div>
                        <div class="panel-body">
                           <div class="form-group ">
                              <label class="col-sm-3 control-label">Nama Lengkap* :</label>
                              <div class="col-sm-6">
                                <input
                                    type="text"
                                    class="form-control"
                                    placeholder="Nama Sesuai Akta Kelahiran / Ijazah"
                                    name="nama"
                                    value="{{ old('nama') ?? '' }}"
                                    onkeyup="app.uppercase(this)"
                                    required
                                    autofocus>
                                    {!! $errors->first('nama', '<small class="form-text text-danger">:message</small>') !!}
                                 <span class="help-block">Nama di awali huruf besar di awal dan setelah spasi contoh benar : Abc,Def, contoh
                                 salah : abc,def,ABC,DEF .</span>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-3 control-label">Tempat Lahir* :</label>
                              <div class="col-sm-6">
                                 <input onkeyup="app.uppercase(this)" class="form-control required" placeholder="Isi Kotak Tempat Lahir" name="tempat_lahir" type="text"><br>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-3 control-label">Tanggal Lahir* :</label>
                              <div class="col-sm-6">
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <input class="form-control required" placeholder="dd" maxlength="2" name="tanggal_lahir" type="number">
                                    </div>
                                    <div class="col-xs-4">
                                       <select class="form-control required" maxlength="2" name="bulan_lahir">
                                          <option selected="selected" value="">mm</option>
                                          <option value="1">Januari</option>
                                          <option value="2">February</option>
                                          <option value="3">Maret</option>
                                          <option value="4">April</option>
                                          <option value="5">Mei</option>
                                          <option value="6">Juni</option>
                                          <option value="7">Juli</option>
                                          <option value="8">Agustus</option>
                                          <option value="9">September</option>
                                          <option value="10">Oktober</option>
                                          <option value="11">November</option>
                                          <option value="12">Desember</option>
                                       </select>
                                    </div>
                                    <div class="col-xs-4">
                                       <input class="form-control required" placeholder="yyyy" maxlength="4" name="tahun_lahir" type="number">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <label class="col-sm-3 control-label">Jenis Kelamin* :</label>
                              <div class="col-sm-6">
                                 <label class="checkbox-inline">
                                 <input name="jk" type="radio" value="L">
                                 Laki-Laki
                                 </label>
                                 <label class="checkbox-inline">
                                 <input name="jk" type="radio" value="P">
                                 Perempuan
                                 </label>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-sm-3 control-label">Asal Sekolah* :</label>
                              <div class="col-sm-6">
                                 <input onkeyup="app.uppercase(this)" class="form-control required" placeholder="isi Nama Sekolah Asal" name="asal_sekolah" type="text">
                                 <span class="help-block">Untuk Pendaftar KB/TK Isi dengan NA</span>
                              </div>
                           </div>

                           <div class="form-group ">
                            <label class="col-sm-3 control-label">Pilihan Program* :</label>
                            <div class="col-sm-6">
                              <select class="form-control required" name="program">
                                  <option selected="selected" value="null">Pilih Program Kelas</option>
                                  <option value="0">Kelas Tahfidz</option>
                                  <option value="1">Kelas IT</option>
                                  <option value="2">Reguler</option>
                              </select>
                            </div>
                         </div>

                           <div class="form-group ">
                              <label class="col-sm-3 control-label">Kategori Pendaftar* :</label>
                              <div class="col-sm-6">
                                <select class="form-control required" name="kategori">
                                    <option selected="selected" value="null">Pilih Kategori Pendaftar</option>
                                    <option value="0">Umum</option>
                                    <option value="1">Pendiunan PT Badak</option>
                                    <option value="2">PT Badak</option>
                                    <option value="3">YPVDP</option>
                                    <option value="4">Mitra PT Badak</option>
                                </select>
                              </div>
                           </div>

                           <div class="form-group">
                              <label class="col-sm-3 control-label">No Hp* :</label>
                              <div class="col-sm-6">
                                 <input class="form-control required" placeholder="isi Nomor HP / WA" name="no_hp" type="text">
                              </div>
                           </div>

                           <div class="form-group">
                              <label class="col-sm-3 control-label">Alamat E-mail* :</label>
                              <div class="col-sm-6">
                                 <input class="form-control required" placeholder="isi Alamat E-mail" name="email" type="email">
                              </div>
                           </div>

                           <div class="form-group">
                              <label class="col-sm-3 control-label">Password* :</label>
                              <div class="col-sm-6">
                                 <input class="form-control required" placeholder="isi Password untuk login" name="password" type="password">
                              </div>
                           </div>

                            <div class="form-group text-center">
                                <button
                                    type="submit"
                                    class="btn btn-primary">
                                    Daftar
                                </button>
                            </div>
                        </div>
                        <script src="{{ asset('assets/js/selectFilter.js') }}" type="text/javascript"></script>
                     </div>
                     <div id="report_score">
                     </div>
                     <div id="enquirer">
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="footer">
         <div class="container">
            <div class="row">
               <div class="col-md-12 text-center">
                  <p>PSB. {{ date('Y') }}. by Cosmolab Digital</p>
               </div>
            </div>
         </div>
      </div>
      <div class="modal">
         <!-- Place at bottom of page -->
      </div>
      <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('assets/js/scrollBack.js') }}"></script>
      <script type="text/javascript">
         $(document).ready(function () {
             $body = $("body");
             scrollBack(".content");
             $("body").on('click', 'a#next, a#back, button#next, a#choose-payment, input#btn-payment-model', function (e) {
                 $body.addClass("loading");
             });
         });
      </script>
      <script src="{{ asset('assets/js/modernizr-custom.js') }}" type="text/javascript"></script>
      <script type="text/javascript">
         $(document).ready(function () {
             $(function(){
                 if (!Modernizr.inputtypes.date) {
                     $.getScript("{{ asset('assets/js/jquery-ui.js') }}");
                 // If not native HTML5 support, fallback to jQuery datePicker
                     $('input[type=date]').datepicker({
                         // Consistent format with the HTML5 picker
                             dateFormat : 'dd-mm-yy'
                         },
                         // Localization
                         $.datepicker.regional['id']
                     );
                 }
             });
         });
      </script>
   </body>
</HTML>
