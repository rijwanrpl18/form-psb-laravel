<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('nama');
            $table->string('email');
            $table->integer('kategori')->nullable()->comment('1=PT badak, 2=Pensiunan pt badak, 3=ypvdp, 4=mitra pt badak, 0=umum');
            $table->integer('program')->default(3)->comment('1=kelas tahfidz, 2=kelas IT, 0 = reguler');
            $table->boolean('jk')->default(0);
            $table->string('nisn')->nullable();
            $table->string('asal_sekolah')->nullable();
            $table->string('nik')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('agama')->default(0)->comment('0 = islam, 1=kristen, 2= katolik, 3=hindu, 4=budha');
            $table->boolean('kebutuhan_khusus')->default(0)->comment('0 = tidak, 1 = ya');
            $table->text('alamat')->nullable();
            $table->string('rt')->nullable();
            $table->string('rw')->nullable();
            $table->bigInteger('kelurahan_id')->nullable()->comment('ambil relasi ke table area');
            $table->bigInteger('kecamatan_id')->nullable()->comment('ambil relasi ke table area');
            $table->bigInteger('kota_id')->nullable()->comment('ambil relasi ke table area');
            $table->bigInteger('provinsi_id')->nullable()->comment('ambil relasi ke table area');
            $table->string('kode_pos')->nullable();
            $table->tinyInteger('alat_transportasi')->default(0)->comment('0=umum, 1=kendaraan probadi, 2=antar jemput');
            $table->tinyInteger('jenis_tinggal')->default(0)->comment('0=lainnya, 1=bersama orang tua, 2=ikut kerabat');
            $table->string('telepon_rumah')->nullable();
            $table->string('telepon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
