<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodik', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->double('tinggi_badan')->default(0);
            $table->double('berat_badan')->default(0);
            $table->double('lingkar_kepala')->default(0);
            $table->integer('jarak')->default(0)->comment('jarak tempat tinggal ke sekolah dalam meter');
            $table->integer('waktu_tempuh')->default(0)->comment('waktu tempuh berangkat ke sekolah');
            $table->tinyInteger('jumlah_saudara')->default(0)->comment('jumlah_saudara_kandung');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodik');
    }
}
